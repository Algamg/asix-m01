#! /bin/bash 
# Alejandro Gambín Gómez
# Abril 2020
# Crea alumnes del curs que rep com a primer argument.
# Rep un segon argument o més arguments corresponents a noms d'usuaris 
# a donar d'alta en aquesta clase. Cada argument és un login 
# A cada usuari se li assigna un password random de 8 dígits/caracters.
# ------------------------------------------------------------------------

ERR_NARGS=1

if [ $# -lt 2 ];then
  echo "Error: nº args incorrecte"
  echo "Usage: $0 curs file"
  exit $ERR_NARGS
fi

curs=$1
shift
groupadd $curs &> /dev/null

for alumnes in $* 
do
   useradd $alumnes -g $curs -G users -b /home/inf/hisx1/ &> /dev/null
   echo "$alumnes:$(pwgen 8 1)" | tee -a password.log | chpasswd
done
exit 0

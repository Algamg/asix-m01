#! /bin/bash 
# Alejandro Gambín Gómez
# Abril 2020
# Crea alumnes del curs que rep com a primer argument.
# Rep un segon argument corresponent a un fitxer amb noms:password
# d'alumnes a donar d'alte
# 
# Es tracta de crear la classe amb el nom indicat per el primer argumnet i 
# afegir-hi els alumnes que conté el fitxer rebut com a segon argument.
# ------------------------------------------------------------------------

ERR_NARGS=1

if [ $# -ne 2 ];then
  echo "Error: nº args incorrecte"
  echo "Usage: $0 curs file"
  exit $ERR_NARGS
fi


curs=$1
groupadd $curs &> /dev/null

while read -r line
do
  alumnes=$(echo $line | cut -d: -f1)
  passwrd=$(echo $line | cut -d: -f2)

  useradd $alumnes -m -g $curs -G users -b /home/inf/hisx1/ &> /dev/null
  echo "$alumnes:alum" | chpasswd
done < $2
exit 0

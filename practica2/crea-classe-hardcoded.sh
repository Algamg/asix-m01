#! /bin/bash 
# Alejandro Gambín Gómez
# Abril 2020
# Crear 30 alumnes del curs que rep com a argument. 
# Podeu generar els noms dels alumnes automatizadament amb brace expansion hisx-{1-30}
# Als alumnes els assignen el passwd 'alum' per defect
# -----------------------------------------

ERR_NARGS=1

if [ $# -ne 1 ];then
  echo "Error: nº args incorrecte"
  echo "Usage: $0 curs"
  exit $ERR_NARGS
fi
curs=$1
noms=$(echo $curs-{01..30})
groupadd $curs &> /dev/null


for alumnes in $noms
do
  useradd $alumnes -n -g $curs -G users -b /home/inf/hisx1/ &> /dev/null
  echo "$alumnes:alum" | chpasswd
done

exit 0

******************************************************************************
  M01-ISO Sistemes Operatius
  UF2: Gestió de la informació i recursos de xarxa
******************************************************************************
  A04-10-Administració de disc
  Exercici 28: Exercicis de particions i sistemes de fitxers
        - particions, 
  
******************************************************************************

1)  Crear un disc virtual de 100M de tipus Vfat, nom Disc1.img.
    Assignar-li etiqueta disc-dos.
    Muntar/desmuntar a mnt.
    # dd if=/dev/zero of=Disc1.img bs=1M count=100
    # mkfs.vfat Disc1.img
    # fatlabel /dev/loop5 disc-dos
    # mount Disc1.img /mnt 
    # umount /mnt

2)  Crear un disc virtual de 200M de tipus ext3, nom Disc2.img.
    Assignar-li etiqueta linux-prova.
    Muntar/desmuntar a mnt.
    # dd if=/dev/zero of=Disc2.img bs=1M count=200
    # mkfs.ext3 Disc2.img
    # e2label /dev/loop6 linux-prova
    # mount Disc2.img /mnt
    # umount /mnt

3)  Crear un disc virtual de 300M de tipus ntfs, nom Disc3.img.
    Assignar-li etiqueta win-disc.
    Muntar/desmuntar a mnt.
    # dd if=/dev/zero of=Disc3.img bs=1 count=300
    # mkfs.ntfs Disc3.img
    # ntfslabel /dev/loop7 win-disc
    # mount Disc3.img /mnt
    # umount /mnt

4)  Identificar ordres i paquets
    Llista totes les ordres que contenen la cadena label
    Llista totes les ordres que tenen la cadena ntfs
    Identifica el paquet al que pertany la ordre ntfslabel    
    

5) Mount manual
    Muntar la partició de la tarda a /mnt
    Tornar-la a muntar (en calent) ara només coma a root.
    # mount /dev/sda6 /mnt


6) Els devices loop
    Muntar usant losetup el Disc1.img a /tmp/d1
    Muntar directament usant únicament mount el Disc2.img a /tmp/d2
    # losetup /dev/loop5 Disc1.img 
    # mount /dev/loop5 /tmp/d1
    # mount Disc2.img /tmp/d2

7) Imatges iso
   Localitza una imatge .iso (a gandhi).
   Muntar-la a /tmp/iso. navegar per el seu contingut. Fer-ne un tree. 
   
8) Label i uuid
   Definir el muntatge automàtic de Disc1.img a /tmp/d1 per label
   Definir el muntatge automàtic de Disc2.img a /tmp/d2 per uuid
   Definir el muntatge automàtic de Disc3.img a /tmp/d3 però que no 
   es munti automàticament sinó que qualsevol usuari ho pugui fer quan
   vulgui.
   Comprovar-ho rebotant el sistema! 
   # First of all 
   # cp /etc/fstab /home/alejandro/Documents/fstab.bk
   
   #LABEL=disc-dos /tmp/d1                 vfat	  defaults     0 0
   #UUID=164b0501-9457-4857-97c9-f39f8549fcfb /tmp/d2   ext3   defaults      0 0   
   #/dev/loop7  /tmp/d3                    ntfs    users,noauto      0 0
  

   9) Imatges iso (per ftp a gandhi en trobareu a pub/images)
      Definir el muntatge automatitzat d'una imatge iso a /tmp/iso de
      manera que qualsevol usuari pugui muntar i qualsevol altre usuari
      pugui desmuntar.


   10) Xequeig de particions
       fer un xequeig de la partició de la tarda
       fer un xequeig de cada un dels disc virtuals (Disc1.img, Disc2.img i Disc3.img).

       #Per utilitzar la comanda fsck mai s'utilitzara amb el sistema de fitzers muntat.
       #umount /tmp/d1
       #umount /tmp/d2
       #umount /tmp/d3
       #fsck -cv /dev/sda6
       #fsck -cv /dev/loop5
       #fsck -cv /dev/loop6
       #fsck -cv /dev/loop7

    11) Activar/Desactivar, Llistar swap 
        Llistar els swaps actius.
	Desactivar el swap manualment.
	# Swaps actius
	#swapon -s 
	# Desactivar swap
        #swapoff /dev/sda7

    12) Swap usant el loop
        Assignar a Disc2.img el format swap usant el deu device de loop
        Activar manaulment aquest swap. llistar els swaps.
	# swapon /dev/loop6
	# swapon -a 
	# swapon -s 


    13) Swap usant directament un fitxer qualsevol.
	Assignar al fitxer Disc3.img el format swap (directament al fitxer)
	Activar manualment el swap. Llistar els Swap.
	Desactivar tots els swap.

  	# swapon Disc3.img
	# swapon -s
	# swapoff -a

    14) Activar els swap automàticament
	Configurar fstab per tal d'utilitzar els tres swaps.
	Comprovar-ho arrancant de nou!.
	Restaura els swaps a la configuració per defecte de l'aula.
	
	#vim /etc/fstab
        #LABEL=disc-dos /tmp/d1                 swap   defaults     0 0
   	#UUID=164b0501-9457-4857-97c9-f39f8549fcfb /tmp/d2   swap   defaults      0 0
   	#/dev/loop7  /tmp/d3                    swap    users,noauto      0 0
	#reboot
	# Esborrem últimes 3 líneas /etc/fstab

    15) Anàlisi de l'espai dels devices
	1) Llistar l'espai total i el disponible de cada partició muntada, tot
	mostrant el sistema de fitxers que utilitza. Cal mostrar la info en unitas K, M o G.
	2) Llistar només els sistemes de fitxers ext3.
	3) Llistar només els sistemes de fitxers virtuals. Quins són?
	4) Llistar l'ocupació d'inodes només dels sistemes locals.
	
	1. df -hT 
	2. df -ht ext3
	3. df -haT (Són els sistemes muntats dins de /sys o /proc
	4. df -ilh


    16) Llistat de l'ocupació de l'espai de disc.
	1) Llistar (K, M o G) l'ocupació de disc de /boot.
	2) Llistar l'ocupació de disc de /etc resumint a un nivell de profunditat.
	3) Llistar la ocupació de /etc, /boot i /tmp sumaritzant, però sense
	   incloure en el còmput els fitxers .img.
	
	1. du -sh /boot
        2. du -sh /etc --max-depth=1
	3. du -hsc --exclude=*.img /etc /boot /tmp

    17) Detectar fitxers oberts
	Llistar quins fitxers tenim oberts en la partició matí
	Muntar la partició de la tarda a /mnt. Fer actiu aquest directori
	i llistar els fitxers oberts d'aquesta partició. 
	
	# lsof 
	# mount /dev/sda6 /mnt && cd /mnt
	# lsof /mnt 
	

    18) Ordre stat
	Aplica l'ordre stat a un fitxer, a un directori, a un link a cada un
	dels discs virtuals que hem creat. 
	Aplicala també a /dev/sda6.
	
	# stat alumnes.txt
	# stat /home/Alejandro
	# stat alumne-s.txt
	# stat /dev/sda6




    19) Ordres vàries.
	- Prova les ordres: free, vmstat, top, uptime, hdparam.
	- Llistar la informació que descriu l'estructura del sistema de
	fitxers d'una partició (blocs, inodes, features, mount count, etc).
	- Assignar a /dev/sda6 que cada 80 muntatges o cada 130 dies es faci
	el xequeig automatitzat. Comprovar aquests canvis.
	
	# dumpe2fs -h /dev/sda5
	# tune2fs -c 80 -i 130 /dev/loop6



     20) Enllaços durs i enllaços simbòlics.
	- Crear un fitxer, un enllaç dur a aquest fitxer i un de simbòlic.
	- Llistar els inodes dels tres fitxers. Fer l'ordre stat a tots tres.
	- Usar find per localitzar tots els fitxers que corresponen a un mateix hard link.
	- Usar find per localitzar tots els fitxers que corresponen a un mateix enllaç simbòlic. 
	
	1. $touch links
 	   $ln -s links symlink
	   $ln links hardlink
	2. $stat links
   		 File: links
  		 Size: 0         	Blocks: 0          IO Block: 4096   regular empty file
		Device: fd02h/64770d	Inode: 398311      Links: 2
		Access: (0664/-rw-rw-r--)  Uid: ( 1000/alejandro)   Gid: ( 1000/alejandro)
		Context: unconfined_u:object_r:user_home_t:s0
		Access: 2020-04-24 16:40:55.866505714 +0200
		Modify: 2020-04-24 16:40:55.866505714 +0200
		Change: 2020-04-24 16:41:46.562807699 +0200
 	         Birth: 2020-04-24 16:40:55.866505714 +0200
	     
	   $stat symlink
    	        File: symlink -> links
  		Size: 5         	Blocks: 0          IO Block: 4096   symbolic link
	       Device: fd02h/64770d	Inode: 394197      Links: 1
	       Access: (0777/lrwxrwxrwx)  Uid: ( 1000/alejandro)   Gid: ( 1000/alejandro)
	       Context: unconfined_u:object_r:user_home_t:s0
	       Access: 2020-04-24 16:41:32.921995513 +0200
	       Modify: 2020-04-24 16:41:32.920995527 +0200
	       Change: 2020-04-24 16:41:32.920995527 +0200
 		Birth: 2020-04-24 16:41:32.920995527 +0200
	    
	   $stat hardlink
		File: hardlink
  		Size: 0         	Blocks: 0          IO Block: 4096   regular empty file
	       Device: fd02h/64770d	Inode: 398311      Links: 2
	       Access: (0664/-rw-rw-r--)  Uid: ( 1000/alejandro)   Gid: ( 1000/alejandro)
	       Context: unconfined_u:object_r:user_home_t:s0
	       Access: 2020-04-24 16:40:55.866505714 +0200
	       Modify: 2020-04-24 16:40:55.866505714 +0200
	       Change: 2020-04-24 16:41:46.562807699 +0200
 	        Birth: 2020-04-24 16:40:55.866505714 +0200

	  $ find / -samefile links
	  $ find / -lname links


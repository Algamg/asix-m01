#! /bin/bash 
# Alejandro Gambín Gómez
# Abril 2020
# Funcions size 
# ------------------------------------
# (1)fsize Donat un login calcular amb du l'ocupació del home de l'usuari. 
#  Cal obtenir el home del /etc/passwd.

function fsize(){
	
 
  login=$1
  home=$(grep "^$login:" /etc/passwd | cut -d: -f6) 
  du -sh $home | tr -s '[:blank:]' ' '| cut -d' '  -f1 2>> /dev/null
}


# (2)loginargs Aquesta funció rep logins i per a cada login es mostra l'ocupació de
# disc del home de l'usuari usant fsize. Verificar que es rep almenys un argument.
# Per a cada argument verificar si és un login vàlid, si no generra una traça d'error
   
function loginargs(){
  if [ $# -lt 1 ];then
    echo "Error: nº args incorrecte"
    echo "Usage: $0 logins[...]"
    return 1
  fi

  for logins in $*
  do
    grep "^$logins:" /etc/passwd &> /dev/null
    if [ $? -ne 0 ];then
      echo "Error: El login no existeix" 2>> /dev/stderr
    fi
    fsize $logins
  done
}

# (3)loginfile Rep com a argument un nom de fitxer que conté un lògin per línia.
# Mostrar l'ocupació de disc de cada usuari usant fsize. Verificar que es rep un
# argument i que és un regular file.

function loginfile(){
  if [ $# -ne 1 ];then
    echo "Error: nº args incorrecte"
    echo "Usage: $0 loginsfile"
    return 1
  fi
  
  if [ ! -f $1 ];then
    echo "Error: No es un regular file"
    echo "Usage: $0 loginsfile"
    return 2
   fi
  while read -r login
  do
    fsize $login
  done < $1
}

# (4)loginboth Rep com a argument un file o res (en aquest cas es processa stdin).
# El fitxer o stdin contenen un lògin per línia. Mostrar l'ocupació de disc del home de
# l'usuari. Verificar els arguments rebuts. verificar per cada login rebut que és vàlid

function loginboth(){

  if [ $# -gt 1 ];then
    echo "Error: nº args incorrecte"
    echo "Usage: $0 loginsfile"
    return 1
  fi
  fileIn=/dev/stdin
  if [ $# -eq 1 ];then
    if [ ! -f $fileIn ];then
      echo "Error: No es un regular file"
      echo "Usage: $0 (stdin|loginsfile)"
      return 2
    fi
    fileIn=$1
  fi
  while read -r login
  do
    loginargs $login
  done < $fileIn
}

# (5)grepgid Donat un GID com a argument, llistar els logins dels usuaris que
# petanyen a aquest grup com a grup principal. Verificar que es rep un argument i
# que és un GID vàlid.

function grepgid(){
  
  if [ $# -ne 1 ];then
    echo "Error: nº args incorrecte"
    echo "Usage: $0 GID"
    return 1 
  fi
  gid=$1
  grep "^[^:]*:[^:]*:[^:]*:$gid:" /etc/passwd | cut -d: -f1 &> /dev/null
  if [ $? -ne 0 ];then
    echo "Error: $gid no vàlid"
    echo "Usage: $0 GID"
    return 2
  fi
  logins=$(grep "^[^:]*:[^:]*:[^:]*:$gid:" /etc/passwd | cut -d: -f1)
  echo $logins
}

# (6)gidsize Donat un GID com a argument, mostrar per a cada usuari que pertany
# a aquest grup l'ocupació de disc del seu home. Verificar que es rep un argument i 
# que és un gID vàlid

function gidsize(){

  if [ $# -ne 1 ];then
    echo "Error: nº args incorrecte"
    echo "Usage: $0 GID"
    return 1
  fi
  gid=$1
   
  principalLogins=$(grepgid $gid)
  pertenecenLogins=$(grep "^[^:]*:[^:]*:$gid:" /etc/group | cut -d: -f4 | tr -s ',' ' ' )
  
  for Logins in $principalLogins
  do
    echo "$Logins"
    loginargs $Logins
  done
  for Logins in $pertenecenLogins
  do 
    echo "$Logins"
    loginargs $Logins
  done
}

# (7)allgidsize Llistar de tots els GID del sistema (en ordre creixent) l'ocupació del
# home dels usuaris que hi pertanyen.

function allgidsize(){

  sortedGid=$(sort -t: -k3n,3 /etc/group | cut -d: -f3)
 
  for gid in $sortedGid
  do
    logins=$(grep "^[^:]*:[^:]*:[^:]*:$gid:" /etc/passwd | cut -d: -f1)
    for login in $logins
    do
      echo "$login: $(fsize $login 2> /dev/null)"
    done
  done
}

# (8)allgroupsize Llistar totes les línies de /etc/group i per cada llínia llistar
# l'ocupació del home dels usuaris que hi pertanyen. Ampliar filtrant només els grups
# del 0 al 100.

function allgroupsize(){
  MAX=100
  GID=$(sort -t: -k3n,3 /etc/group | cut -d: -f3)
  for gid in $GID
    do
      if [ $gid -le $MAX ];then
        logins=$(grep "^[^:]*:[^:]*:[^:]*:$gid:" /etc/passwd | cut -d: -f1)
      
        for login in $logins
        do
          echo "$login: $(fsize $login 2> /dev/null)"
        done
      fi  
  done
}


# (9)fstype Donat un fstype llista el device i el mountpoint (per odre de device) de
# les entrades de fstab d'aquest fstype.

function fstype(){
  
  fstype=$1
  tr -s '[:blank:]' ':' < /etc/fstab | grep "^[^:]*:[^:]*:$fstype:" | cut -d: -f1,2 | sort -t: -k1,1 -k2,2
}


# (10)allfstype LLista per a cada fstype que existeix al fstab (per ordre lexicogràfic)
# i les entrades d'quest tipus. Llistar tabuladament el device i el mountpoint.

function allfstype(){

  egrep -v "(^#|^$)" /etc/fstab	| tr -s '[:blank:]' '\t' | cut -d$'\t' -f1,2 | sort -t\t -k3,3 
}

# (11)allfstypeif LLista per a cada fstype que existeix al fstab (per ordre lexicogràfic)
# les entrades d'quest tipus. Llistar tabuladament el device i el mountpoint. Es rep un
# valor numèric d'argument que indica el numéro mínim d'entrades d'aquest fstype
# que hi ha d'haver per sortir al llistat.

function allfstypeif() {
  if [ $# -ne 1 ];then
    echo "Error: nº args incorrecte"
    echo "Usage: $0 number"
  fi
  minim=$1
  nEntrades=$(egrep -v "(^#|^$)" /etc/fstab | tr -s '[:blank:]' '\t' | cut -d$'\t' -f1,2 | sort -t\t -k3,3 | wc -l)
  if [ $minim -ge $nEntrades ];then   
    egrep -v "(^#|^$)" /etc/fstab | tr -s '[:blank:]' '\t' | cut -d$'\t' -f1,2 | sort -t\t -k3,3
  fi

}



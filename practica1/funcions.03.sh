#! /bin/bash 
# Alejandro Gambín Gómez
# Abril 2020
# Funcions repàs 
# ------------------------------------
# 9. showAllGroupMainMembers
# Llistar per ordre de gname tots els grups del sistema, per a cada grup hi ha una
# capçalera amb el nom del grup i a continuació el llistat de tots els usuaris que
# tenen aquell grup com a grup principal, ordenat per login.
# ------------------------------------ 
function showAllGroupMainMembers(){
 # Guardamos el gname ordenado alfabeticamente 
 llista_gnames=$(cut -d: -f1 /etc/group | sort -u) 
 
  for gname in $llista_gnames
  do
   gid=$(grep "^$gname:" /etc/group | cut -d: -f3)
   grep "^[^:]*:[^:]*:[^:]*:$gid:" /etc/passwd &> /dev/null
   if [ $? -eq 0 ];then
     echo "Gname: $gname"
     grep "^[^:]*:[^:]*:[^:]*:$gid:" /etc/passwd | cut -d: -f1 | sort 
   fi
 
  done
}
# ----------------------------------------
# 11.showAllGroupMainMembers2
# Fer una nova versió de showAllGroupMembers on per cada grup es llisti una
# capçalera amb el nom del grup i la quantitat d’usuaris que tenen aquest grup com
# a grup principal. Per a cada grup llavors es llisten les línies de detall dels usuaris
# que hi pertanyem, per ordre de login, mostrant login, uid, home i shell.
function showAllGroupMainMembers2(){


  llista_gnames=$(cut -d: -f1 /etc/group | sort -u)

  for gname in $llista_gnames
  do
    gid=$(grep "^$gname:" /etc/group | cut -d: -f3)
    comptador=$(grep "^[^:]*:[^:]*:[^:]*:$gid:" /etc/passwd | wc -l)
    if [ $comptador -ne 0 ];then
      echo "Gname: $gname($comptador)"
      grep "^[^:]*:[^:]*:[^:]*:$gid:" /etc/passwd | cut -d: -f1,3,6,7 | sort | sed -r 's/^(.*)$/ \1/' | tr -s ':' ' '
    fi
  done
}
# -----------------------------------------------
# 18.showGidMembers [file]
# Processa un fitxer de text que conté un gid per línia o bé stdin (on també espera
# rebre un gid per línia). Mostra el llistat dels usuaris (login, uid, home) d’aquells
# usuaris que tenen aquest grup com a grup principal.


function showGidMembers(){
  if [ $# -gt 1 ];then
    echo "Error: nº args incorrecte"
    echo "Usage: $0 (stdin|file)"
    return 1
  fi 
  
  fileIn=/dev/stdin 
 # Grup principal = /etc/group
  if [ $# -eq 1 ];then
    if ! [ -f $1 ];then 
      echo "Error: $1 no es un regular file"
      echo "Usage: $0 (stdin|file)"
      return 1
     fi
     fileIn=$1
  fi
  while read -r gid
  do
     grep "^[^:]*:[^:]*:[^:]*:$gid:" /etc/passwd | cut -d: -f1,3,6
  done < $fileIn
}
# 19.showGidMembers2 [file]
# Processa un fitxer de text que conté un gid per línia o bé stdin (on també espera
# rebre un gid per línia). Mostra el llistat dels usuaris (login, uid, home) d’aquells
# usuaris que tenen aquest grup com a grup principal. Fa un having, mostrant
# només els grups si hi ha almenys 3 usuaris al grup.

function showGidMembers2(){

  if [ $# -gt 1 ];then
    echo "Error: nº args incorrecte"
    echo "Usage: $0 (stdin|file)"
    return 1
  fi

  fileIn=/dev/stdin
  
  if [ $# -eq 1 ];then
    if ! [ -f $1 ];then
      echo "Error: $1 no es un regular file"
      echo "Usage: $0 (stdin|file)"
      return 1
     fi
    fileIn=$1
  fi
 
 
    while read -r gid
    do
      comptador=$(grep "^[^:]*:[^:]*:[^:]*:$gid:" /etc/passwd | cut -d: -f1,3,6 | wc -l)
      if [ $comptador -ge 3 ];then
        grep "^[^:]*:[^:]*:[^:]*:$gid:" /etc/passwd | cut -d: -f1,3,6
      fi
  done < $fileIn

}
#20.showPedidos oficina
# Donat un número d’oficina com a argument (validar es rep un argument i que és
# un número d’oficina vàlid), obtenir els codis dels representants de vendes que
# treballen en aquesta oficina i llistar les comandes d’aquest repventas. És a dir,
# estem fent un llistat de les comandes agrupades per venedors dels venedors que
# treballen en una determinada oficina.

function showPedidos(){
  if [ $# -ne 1 ];then
    echo "Error: nº args incorrecte "
    echo "Usage: $0 nºoficia "
    return 1
  fi
  oficina=$1
  
  egrep "^$oficina[[:blank:]]" oficinas.dat &> /dev/null
  if [ $? -ne 0 ];then
    echo "Error: El número d'oficina no existeix" 
    echo "Usage: $0 oficina"
    return 2
  fi
  representantes=$(tr -s '\t' ':'  < repventas.dat | egrep "^[^:]*:[^:]*:[^:]*:$oficina:" | cut -d: -f1)
  
  for num_empl in $representantes
  do
    tr -s '\t' ':'  < pedidos.dat | egrep "^[^:]*:[^:]*:[^:]*:$num_empl:"
  done

}

#21.getHome login
# Fer una funció que rep un login (no cal validar que rep un argument ni que sigui
# un login vàlid) i mostra per stdout el home de l’usuari.
# La funció retorna 0 si ha pogut trobar el home i diferent de zero si no l’ha pogut trobar.

function getHome(){
  login=$1	
  grep "^$login:" /etc/passwd &> /dev/null
  if [ $? -ne 0 ];then
    echo "Error: $login no es un login valido"
    return 1
  else 
    grep "^$login:" /etc/passwd | cut -d: -f6  
  fi
  return 0
}

# 22.getHoleList login[...]
# Fer una funció que rep un o més login d’arguments i per a cada un d’ells mostra
# el seu home utilitzant la funció getHome. Cal validar els arguments rebuts.

function getHoleList(){ 
  if [ $# -lt 1 ];then
    echo "Error: nº args incorrecte "
    echo "Usage: $0 login "
    return 1
  fi
  
  grep "^$login:" /etc/passwd &> /dev/null
  if [ $? -ne 0 ];then
    echo "Error: $1 no es un login valido"
    echo "Usage: $0 login"
  fi
 
  for login in $*
  do
    getHome $login
  done  
}

# 23.getSize homedir
# Donat un homedir com a argument mostra per stdout l’ocupació en bytes del directori. 
# Per calcular l’ocupació utilitza du del que només volem el resultat numèric.
# Cal validar que el directori físic existeix (recordeu que hi ha usuaris que
# poden tenir de home valors que no són rutes vàlides com /bin/false). 
# Retorna 0 si el directori existeix i un valor diferent si no existeix. 
# Cal usar les funcions que estem creant.

function getSize(){
  
  homedir=$1
  grep "^[^:]*:[^:]*:[^:]*:[^:]*:[^:]*:$homedir:" /etc/passwd &> /dev/null
  if [ $? -ne 0 ];then 
    echo "Error: $homedir no existeix"
    echo "Usage: $0 homedir"
    return 1
   else
     du -s -b /home/alejandro/ | tr -s '[:blank:]' ' ' | cut -d' '  -f1
  fi 
}

# 24. getSizeIn
# Ídem exercici anterior però processa un a un els login que rep per stdin, cada
# línia un lògin. Valida que el login existeixi, si no és un error. Donat el login mostra
# per stdout el size del seu home. Cal usar les funcions que estem definint.

function getSizeIn(){
  fileIn=/dev/null
  
  if [ $# -gt 1 ];then
    echo "Error: Nº args incorrecte"
    echo "Usage: $0 [login]"
    return 1 
  fi

  if [ $# -eq 1 ];then
    fileIn=$1
  fi


  while read -r login
  do
    getHome $login &> /dev/null
    if [ $? -ne 0 ];then 
      echo "Error: El $login no existeix"
      echo "Usage: $0 [login]"
      return 2
    else
      home=$(getHome $login)
      getSize $home
    fi
  done
}

# 25. getAllUsersSize
# Processa una a una les línies del fitxer /etc/passwd i per a cada usuari mostra per
# stdout el size del seu home.

function getAllUsersSize(){
  file=/etc/passwd

  while read -r line
  do
    user=$(echo $line | cut -d: -f1)
    homeUser=$(echo $line | cut -d: -f6)
    echo "$user: $(getSize $homeUser)"
  done < $file

}

# 26.showShadow login
# Donat un login mostrar els camps del /etc/shadow d’aquest login, mostrant camp
# a camp amb una etiqueta i el valor del camp. Validar que es rep un argument i
# que el login existeix.

function showShadow(){
  login=$1
  line=$(grep "^$login:" /etc/shadow)
   
  passwd=$(echo "$line" | cut -d: -f2)
  dateLastPsswd=$(echo "$line" | cut -d: -f3)
  minPsswd=$(echo "$line" | cut -d: -f4)
  maxPsswd=$(echo "$line" | cut -d: -f5)
  warnPsswd=$(echo "$line" | cut -d: -f6)
  inactPasswd=$(echo "$line" | cut -d: -f7)
  accountPasswd=$(echo "$line" | cut -d: -f8)
  
 
  echo "Login: $login"
  echo "Password: $dateLastPsswd"
  echo "Date Of last Psswd Change: $minPsswd" 
  echo "Minum password age: $maxPsswd"
  echo "Maximum password age: $warnPsswd"
  echo "Password warning period: $inactPasswd"
  echo "Account expiration date: $accountPasswd" 
}

# 27.showShadowList login[...]
# Mostrar per a cada lògin les dades del seu shadow. Validar que es rep almenys un
# argument i que existeix cada un dels lògins.

function showShadowList(){
  if [ $# -lt 1 ];then
    echo "Error: nº args incorrecte"
    echo "Usage: $0 login[...]"
    return 1
  fi

  for login in $* 
  do
    grep "^$login:" /etc/shadow &> /dev/null
    if [ $? -ne 0 ];then
      echo "Error: El $login no existeix"
      echo "Usage: $0 login[...]"
      return 2
    fi
    
    showShadow $login
    echo "-------------"
  done 
}

# 28.showShadowIn < login
# Mostrar els camps del shadow de cada usuari rebut per l’entrada estàndard. 
# Cada línia de l’entrada estàndard correspon a un login. Verificar que cada un dels lògins existeix.

function showShadowIn(){

  while read -r login
  do
    grep "^$login:" /etc/shadow &> /dev/null
    if [ $? -ne 0 ];then
      echo "Error: El $login no existeix"
      echo "Usage: $0 < login"
      return 1
    fi
    showShadow $login	
    echo " ----------- "
  done 
}





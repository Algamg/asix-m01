#! /bin/bash 
# Alejandro Gambín Gómez
# Abril 2020
# Programa que rep un login i esborrar TOT el que pertany a l'usuari
# pero deixant un tar.gz de tots els fitxers que li pertayen. Va generant 
# una traça per stderr de tot allò que va eliminant
# ------------------------------------
ERR_NARGS=1
ERR_USERNOEXIST=2
if [ $# -ne 1 ];then
  echo "Error: nºargs incorrecte"
  echo "Usage: $0 login"
  exit $ERR_NARGS
fi

login=$1

grep "^$login:" /etc/passwd &> /dev/null
if [ $? -ne 0 ];then
  echo "Error: L'usuari $login no existeix"
  echo "Usage: $0 login"
  exit $ERR_USERNOEXIST
fi

# Fem papilla tots el processos que té executant ara mateix l'usuari
killall -user $login &> /dev/null


tar -czf home$login.tar.gz $(find / -user $login 2> /dev/null) 2> /dev/null


rm $(find / -user $login 2> /dev/null) > /dev/stderr

